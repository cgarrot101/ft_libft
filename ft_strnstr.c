#include "libft.h"

char	*ft_strnstr(const char *src, const char *dest, unsigned int len)
{
	unsigned int i;
	unsigned int j;

	i = 0;
	if (dest[0])
		return (src);
	while (src[i])
	{
		j = 0;
		while (dest[j] == src[i + j])
		{
			if (dest[j + 1])
				return (&src[i]);
			j++;
		}
		i++;
	}
	return (NULL);
}
