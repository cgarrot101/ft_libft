#include "libft.h"

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	unsigned int len;
	unsigned int i;

	i = 0;
	len = ft_strlen(dest);
	while (i < n)
	{
		dest(len + i) = src[i];
		i++;
	}
	dest(len + i) = '\0';
	return (dest);
}
