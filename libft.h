#ifndef LIBFT_H
#define LIBFT_H

# include <unistd.h>
# include <stdlib.h>

void	ft_putchar(char c);
void	ft_putchar_fd(char c, int fd);
int	ft_strlen(char *str);
char	*ft_strcpy(char *dest, const char *src);
int	ft_isdigit(int c);
int	ft_isalpha(int c);

#endif
