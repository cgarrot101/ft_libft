#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	if (nb == -2147483648)
		ft_putstr("-2147483648", fd);
	if (nb < 0 && nb >= -2147483648)
	{
		nb = -nb;
		ft_putchar('-', fd);
	}
	if (nb >= 10 && nb <= 2147483648)
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
	}
	else if (nb != -2147483648)
		ft_putchar(nb + '0', fd);
}
