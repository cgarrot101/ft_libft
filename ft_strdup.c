#include "libft.h"

char	*ft_strdup(const char *str)
{
	char *dup;
	
	if (!(dup = malloc(sizeof(char) * ft_strlen(str) + 1)))
		return (0);
	else
		dup = ft_strcpy(dup, str);
	return (dup);
}
